#pragma once
#include "Shape.h"
#include <math.h>
class CEllipse :
	public CShape
{
public:
	CEllipse();
	~CEllipse();
	CEllipse(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void Draw(Graphics *grp, Pen *pen) {
		if (x2 > x1 && y2 > y1)
			grp->DrawEllipse(pen, x1, y1, x2 - x1, y2 - y1);
		else if (x2 > x1 && y2 < y1)
			grp->DrawEllipse(pen, x1, y2, x2 - x1, y1 - y2);
		else if (x2 < x1 && y2 < y1)
			grp->DrawEllipse(pen, x2, y2, x1 - x2, y1 - y2);
		else
			grp->DrawEllipse(pen, x2, y1, x1 - x2, y2 - y1);
	}

	CShape* Create(int a, int b, int c, int d) {
		return new CEllipse(a, b, c, d);
	}
};

