#pragma once
#include "Shape.h"

class CLine :
	public CShape
{
public:
	CLine();
	CLine(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	~CLine();
	void Draw(Graphics *grp, Pen *pen) {
		grp->DrawLine(pen, x1, y1, x2, y2);
	}

	CShape* Create(int a, int b, int c, int d) {
		return new CLine(a, b, c, d);
	}
};

