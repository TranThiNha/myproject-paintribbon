#pragma once
#include "Shape.h"
#include <math.h>
class CCircle :
	public CShape
{
public:
	CCircle();
	~CCircle();
	CCircle(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	void Draw(Graphics *grp, Pen *pen) {
		int min;
		if (abs(x2 - x1) > abs(y2 - y1))
		{
			min = abs(x2 - x1);
		}
		else min = abs(y2 - y1);
		if (x2 > x1 && y2 > y1)
			grp->DrawEllipse(pen, x1, y1, min,  min);
		else if (x2 > x1 && y2 < y1)
			grp->DrawEllipse(pen, x1, y1, min, -min);
		else if (x2 < x1 && y2 < y1)
			grp->DrawEllipse(pen, x1, y1, -min, -min);
		else
			grp->DrawEllipse(pen, x1, y1, -min, min);
	}

	CShape* Create(int a, int b, int c, int d) {
		return new CCircle(a, b, c, d);
	}
};

