#pragma once
#include <ObjIdl.h>
#include <gdiplus.h>
#include <math.h>
#pragma comment(lib, "gdiplus.lib")
using namespace Gdiplus;

class CShape
{
protected:
	int x1;
	int y1;
	int x2;
	int y2;
public:
	CShape();
	virtual void Draw(Graphics *grp, Pen *pen) = 0;
	void SetData(int a, int b, int c, int d)
	{
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	virtual CShape* Create(int a, int b, int c, int d) = 0;
	~CShape();
};

