Họ tên: Trần Thị Nhã
MSSV: 1512364
Email: trannha132@gmail.com
1. Các chức năng đã làm được:
Xây dựng chương trình vẽ 5 loại hình cơ bản sử dụng Ribbon và GDI+.
- Đường thẳng (line)
- Hình chữ nhật (rectangle).Nếu giữ phím Shift sẽ vẽ hình vuông (Square)
- Hình Ellipse. Nếu giữ phím Shift sẽ vẽ hình tròn (Circle)
- Cho phép chọn loại hình cần vẽ trên Ribbon.
- Mặc định màu đen nét đơn giản.
- Chọn Clear và nhấn vào màn hình sẽ xóa đi các hình đang có trên màn hình.
2. Luồng sự kiện chính:
- Hiện màn hình có Ribbon có nút chọn Type Shape.
- Chọn loại hình cần vẽ trong menu Shape.
- Đè chuột trái và di chuyển để vẽ hình. Buông chuột để có được hình cuối cùng.
- Tiếp tục chọn các loại hình khác trong menu và vẽ tương tự.
- Giữ phím Shift khi đang vẽ hình chữ nhật sẽ vẽ ra hình vuông.
- Giữ phím Shift khi đang vẽ hình ellipse sẽ vẽ ra hình tròn. 
- Chọn Clear và nhấn vào màn hình sẽ xóa đi các hình đang có trên màn hình.
3. Luồng sự kiện phụ:
- Có thể vẽ nhiều loại hình trên màn hình mà không mất đi hình cũ.
4. Link Bitbucket: https://bitbucket.org/TranThiNha/myproject-paintribbon
5. Link Youtube: https://youtu.be/nF-To_vcTG4